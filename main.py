import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d


#Все параметры, кроме x, являются рандомными
def interpolate(x, leftPoint, leftMiddlePoint, centerPoint, rightMiddlePoint, rightBorderPoint):
    return

def IsUnimodalFunction(spline, leftBorder, rightBorder):
    X = np.linspace(leftBorder, rightBorder, int((rightBorder - leftBorder) / 1e-6))
    Y = spline(X)
    difArr = np.diff(Y)
    difArr = difArr > 0
    difArr = np.diff(difArr)
    summm = np.sum(difArr)
    return summm < 2

import random
def GenerateCenterPoint(maxLeft, maxRight):
    return (random.uniform(maxLeft, maxRight), random.uniform(-1, 1))
def GenerateMidlePoints(centerPoint, leftMaxMid, rightMaxMid):
    return (random.uniform(leftMaxMid, centerPoint[0]), random.uniform(centerPoint[1] + 1, 5)),\
           (random.uniform(centerPoint[0], rightMaxMid), random.uniform(centerPoint[1] + 1, 5))
def GenerateBorderPoints(leftMidPoint, rightMidPoint, leftBorder, rightBorder):
    return (leftBorder, random.uniform(leftMidPoint[1] + 1, 10)),\
           (rightBorder, random.uniform(rightMidPoint[1] + 1, 10))


if __name__ == '__main__':
    interpolateXbase = []
    interpolateYbase = []
    while True:
        centerPoint = GenerateCenterPoint(-4, 4)
        leftMid, rightMid = GenerateMidlePoints(centerPoint, -7, 7)
        left, right = GenerateBorderPoints(leftMid, rightMid, -10, 10)
        X = np.array([left[0], leftMid[0], centerPoint[0], rightMid[0], right[0]])
        Y = np.array([left[1], leftMid[1], centerPoint[1], rightMid[1], right[1]])
        f = interp1d(X, Y, kind="cubic")
        #Xnew = np.linspace(-10, 10, 1000)
        isUnimodal = IsUnimodalFunction(f, -10, 10)
        print(isUnimodal)
        if isUnimodal:
            interpolateXbase.append(X)
            interpolateYbase.append(Y)
            #plt.plot(Xnew, f(Xnew), c="black")
            #plt.show()
            #break
        if len(interpolateXbase) > 10:
            break;

    np.save("Xbase", np.array(interpolateXbase))
    np.save("Ybase", np.array(interpolateYbase))


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
