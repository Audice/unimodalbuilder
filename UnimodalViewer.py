import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d

if __name__ == '__main__':
    xbase = np.load("Xbase.npy")
    ybase = np.load("Ybase.npy")

    for i in range(len(xbase)):
        X = xbase[i]
        Y = ybase[i]
        f = interp1d(X, Y, kind="cubic")
        Xnew = np.linspace(-10, 10, 1000)
        plt.plot(Xnew, f(Xnew), c="black")
        plt.show()
